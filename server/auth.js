'use strict';

var LocalStrategy = require("passport-local").Strategy;
var FacebookStrategy = require("passport-facebook").Strategy;
var bcryptjs = require("bcryptjs");
var config = require("./config");
var usertable = require('./usertable.json');

module.exports = function(database, passport){
    
    // 2
    function authenticeTheUser(username, password, done){
        console.log("authenticeTheUser");
        console.log("username : " + username);
        console.log("password : " + password);
        console.log("done : " + done);
        console.log("JSON>  : ");
        var _username = null;

        for(var i=0; i < usertable.length; i++){
            console.log("item> " + usertable[i]);
            if(usertable[i].username === username 
                    && usertable[i].password === password){
                _username = usertable[i].username;
                break;
            }
        }
        console.log(_username);
        if(_username){
            console.log("login is good !");
            done(null, _username);
        }else{
            console.log("login failed !");
            done(null, false);
        }
        
    }

    // 1
    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticeTheUser));

    passport.serializeUser(function(user, done){
        console.log("serializeUser");
        console.log("username : " + user);
        console.log("done : " + done);
        done(null, user); 
    });

    passport.deserializeUser(function(user, done){
        console.log("deserializeUser");
        console.log("username : " + user);
        console.log("done : " + done);
        done(null, user);
    });
}