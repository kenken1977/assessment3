// DEPENDENCIES ------------------------------------------------------------------------------------------------------
// Loads express module and assigns it to a var called express
var express = require("express");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var session = require("express-session");
var passport = require("passport");
var config = require("./config");

// CONSTANTS ---------------------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = config.port;

// OTHER VARS ---------------------------------------------------------------------------------------------------------
//Create an instance of express application
var app = express();

app.use(cookieParser());
// Populates req.body with information submitted through the registration form.
// Expected content type is application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
// Default $http content type is application/json so we use json as the parser type
app.use(bodyParser.json({limit: '50mb'}));

app.use(session({
    secret: "ad7e0b08c4b3b325553c90937111d22a188bcbf3d4375d1d7e176df0e7b814af",
    resave: false,
    saveUninitialized: true
}));

app.use(passport.initialize());
app.use(passport.session());

var database = require("./database");
require("./auth")(database, passport);
require("./routes")(app, database, passport);

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at " + config.domain_name);
});