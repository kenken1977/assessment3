(function () {
    angular
        .module("EMS")
        .controller("RegisterCtrl", ["$sanitize", "$state" , RegisterCtrl]);

    function RegisterCtrl($sanitize, $state){
        var vm = this;
        
        vm.register = function(){
            // TODO you need Auth Service or Factory

            $state.go("SignIn");
        }
        
    }
})();