(function () {
    angular
        .module("EMS")
        .controller("HomePageCtrl", ["$state", "AuthService", HomePageCtrl]);

    function HomePageCtrl($state, AuthService){
        var vm = this;
        vm.profilelink = "/#!/SignIn";
        
        AuthService.isUserLoggedIn(function(result){
            console.log(">>>> >" + JSON.stringify(result));
            if(!result.data){
                vm.emailAddress = result.data;
                vm.profilelink = "/#!/register";
            }
        });

        vm.logout = function(){
            console.log("Logout !");
            AuthService.logout();
            $state.go('SignIn');
        }
    }
})();