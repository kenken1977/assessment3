(function () {
    angular
        .module("EMS")
        .controller("LoginCtrl", ["$state", "AuthService", LoginCtrl]);

    function LoginCtrl($state, AuthService){
        var vm = this;
        
        vm.login = function(){
            AuthService.login(vm.user).then(function(){
                console.log("login ...");
                AuthService.isUserLoggedIn(function(result){
                    if(result){
                        $state.go('register');
                    }else{
                        $state.go('SignIn');
                    }
                });
            }).catch(function(error){
                console.log("erorr !" + error);
            });
            
        }
    }
})();